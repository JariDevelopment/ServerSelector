package nl.dizmizzer.ss.gui;

import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by DizMizzer.
 * Users don't have permission to release
 * the code unless stated by the Developer.
 * You are allowed to copy the source code
 * and edit it in any way, but not distribute
 * it. If you want to distribute addons,
 * please use the API. If you can't access
 * a certain thing in the API, please contact
 * the developer in contact.txt.
 */
public class GUIHolder implements InventoryHolder {

    private Map<Integer, Icon> icons = new HashMap<>();
    private int size = 9;
    private String title = "";

    public GUIHolder(int size, String title) {
        this.size = size;
        this.title = title;
    }

    public void setIcon(int slot, Icon item) {
        icons.put(slot, item);
    }

    @Override
    public Inventory getInventory() {
        Inventory inv = Bukkit.createInventory(this, this.size, this.title);

        for (Map.Entry<Integer, Icon> entry : icons.entrySet()) {
            inv.setItem(entry.getKey(), entry.getValue().getItemStack());
        }
        return inv;
    }

    public Icon getIcon(int rawSlot) {
        if (!icons.containsKey(rawSlot)) return null;
        return icons.get(rawSlot);
    }
}
