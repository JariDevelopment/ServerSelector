package nl.dizmizzer.ss;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import nl.dizmizzer.ss.gui.GUIHolder;
import nl.dizmizzer.ss.gui.Icon;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by DizMizzer.
 * Users don't have permission to release
 * the code unless stated by the Developer.
 * You are allowed to copy the source code
 * and edit it in any way, but not distribute
 * it. If you want to distribute addons,
 * please use the API. If you can't access
 * a certain thing in the API, please contact
 * the developer in contact.txt.
 */
public class EventList implements Listener {

    GUIHolder menu;
    ServerSelector ss;
    private ItemStack compass = new ItemStack(Material.COMPASS, 1);

    EventList(ServerSelector serverSelector) {
        ItemMeta im = compass.getItemMeta();
        im.setDisplayName(ChatColor.AQUA + "" + ChatColor.BOLD + "Selector");
        ArrayList<String> a = new ArrayList<>();
        a.add(toColor("&7Click here to select"));
        a.add(toColor("&7a different server!"));
        im.setLore(a);
        compass.setItemMeta(im);

        this.ss = serverSelector;

        menu = new GUIHolder(27, ChatColor.BLACK + "Select Server: ");
        Icon factionIcon = new Icon(material(Material.IRON_SWORD,
                ChatColor.GREEN + "Factions",
                new String[]{ChatColor.YELLOW + "Click here to go",
                        ChatColor.YELLOW + "to the factions server!"})).addAction(player -> {

            ByteArrayDataOutput out = ByteStreams.newDataOutput();
            out.writeUTF("Connect");
            out.writeUTF("factions");
            player.sendPluginMessage(ss, "BungeeCord", out.toByteArray());
            player.sendMessage(ChatColor.RED + "Sending you to Factions!");
        });

        Icon lobbyIcon = new Icon(material(Material.WORKBENCH,
                ChatColor.RED + "Lobby",
                new String[]{ChatColor.YELLOW + "Click here to go back",
                        ChatColor.YELLOW + "to the lobby!"})).addAction(player -> {
            ByteArrayDataOutput out = ByteStreams.newDataOutput();
            out.writeUTF("Connect");
            out.writeUTF("Lobby");
            player.sendPluginMessage(ss, "BungeeCord", out.toByteArray());
            player.sendMessage(ChatColor.RED + "Sending you to Lobby!");
        });

        Icon townyIcon = new Icon(material(Material.SAPLING,
                ChatColor.GREEN + "Towny",
                new String[]{ChatColor.YELLOW + "Click here to go",
                        ChatColor.YELLOW + "to the Towny server!"})).addAction(player -> {
            ByteArrayDataOutput out = ByteStreams.newDataOutput();
            out.writeUTF("Connect");
            out.writeUTF("lobby");
            player.sendPluginMessage(ss, "BungeeCord", out.toByteArray());
            player.sendMessage(ChatColor.RED + "Sending you to Lobby!");
        });

        Icon minigamesIcon = new Icon(material(Material.CHEST,
                ChatColor.AQUA + "Minigames",
                new String[]{ChatColor.YELLOW + "Click here to go to",
                        ChatColor.YELLOW + "the Minigames server!"})).addAction(player -> {
            ByteArrayDataOutput out = ByteStreams.newDataOutput();
            out.writeUTF("Connect");
            out.writeUTF("minigame");
            player.sendPluginMessage(ss, "BungeeCord", out.toByteArray());
            player.sendMessage(ChatColor.RED + "Sending you to Mini-Games!");

        });

        menu.setIcon(10, lobbyIcon);
        menu.setIcon(12, townyIcon);
        menu.setIcon(14, factionIcon);
        menu.setIcon(16, minigamesIcon);
    }

    private String toColor(String a) {
        return ChatColor.translateAlternateColorCodes('&', a);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        //Fixed position
        if (!e.getPlayer().getInventory().contains(compass)) e.getPlayer().getInventory().addItem(compass);
    }

    @EventHandler
    public void playerDrop(PlayerDropItemEvent e) {


        //Disable the dropping of items
        try {
            if (e.getItemDrop().getItemStack().getItemMeta().getDisplayName().equalsIgnoreCase(
                    compass.getItemMeta().getDisplayName()))
                e.setCancelled(true);

        } catch (NullPointerException ignored) {
        }
    }

    @EventHandler
    public void onMove(InventoryMoveItemEvent e) {
        try {
            if (e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(compass.getItemMeta().getDisplayName())) {
                if (e.getDestination() != e.getSource()) e.setCancelled(true);
            }
        } catch (NullPointerException ignored) {

        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if (e.getPlayer().getItemInHand() == null) return;
        if (!e.getPlayer().getItemInHand().hasItemMeta()) return;
        if (e.getPlayer().getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase(
                compass.getItemMeta().getDisplayName()
        )) {
            e.getPlayer().openInventory(menu.getInventory());
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onClick(InventoryClickEvent e) {
        if (e.getInventory().getHolder() != e.getWhoClicked().getInventory().getHolder()) {
            if (e.getClick().isShiftClick()) e.setCancelled(true);
            if (!(e.getCurrentItem() == null || !e.getCurrentItem().hasItemMeta())) {

                if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(compass.getItemMeta().getDisplayName())) {
                    if (e.getRawSlot() == e.getSlot()) {
                        e.setCancelled(true);
                    }
                }
            }

            if (e.getCursor() == null || !e.getCursor().hasItemMeta()) return;
            if (e.getCursor().getItemMeta().getDisplayName().equalsIgnoreCase(compass.getItemMeta().getDisplayName())) {
                if (e.getRawSlot() == e.getSlot()) {
                    e.setCancelled(true);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onMenuClick(InventoryClickEvent e) {
        if (!(e.getInventory().getHolder() instanceof GUIHolder)) return;
        if (e.getClick().isShiftClick()) e.setCancelled(true);
        if (e.getRawSlot() == e.getSlot()) {
            GUIHolder holder = (GUIHolder) e.getInventory().getHolder();
            holder.getIcon(e.getRawSlot()).execute((Player) e.getWhoClicked());
        }
    }

    private ItemStack material(Material material, String s, String[] list) {
        ItemStack i = new ItemStack(material);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName(s);
        im.setLore(Arrays.asList(list));
        i.setItemMeta(im);
        return i;

    }

}